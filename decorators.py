from functools import wraps
from flask import request, jsonify
import jwt
from service import UserService

user_service = UserService()

class Security:



    @staticmethod
    def access_token_required(f):
        @wraps(f)
        def function(*args, **kwargs):
            access_token = request.args.get("access_token")
            if access_token is None:
                return jsonify(status=False, message="Authentication is required"), 401
            try:
                user_info = jwt.decode(access_token, 'zizu', algorithms=['HS256'])
            except Exception as e:
                return jsonify(status=False, message="Access token is not validated"), 401

            email = user_info.get('email')
            if email is None:
                return jsonify(status=False, message="Access token is not validated"), 401
            elif not user_service.does_exist(email):
                return jsonify(status=False, message="Access token is not validated"), 401


            return f(*args, **kwargs)

        return function