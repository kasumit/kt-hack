from flask import Flask
from flask_sqlalchemy import  SQLAlchemy
import os
import uuid
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.secret_key = str(uuid.uuid1)




__db_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), './store')
database_path_file = 'sqlite:////{}/test.db'.format(__db_path)

app.config['SQLALCHEMY_DATABASE_URI'] = database_path_file

# Configurations

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# ------------------------------------------------------------------


database = SQLAlchemy(app)

import model, controller


database.create_engine(database_path_file)
database.create_all()


if __name__ == '__main__':
    app.run(host='0.0.0.0')