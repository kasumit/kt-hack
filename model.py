from app import app
from app import database
from werkzeug.security import generate_password_hash, check_password_hash
import uuid

class User(database.Model):
    __tablename__ = 'user'

    id = database.Column(database.Integer, autoincrement=True, primary_key=True)
    name = database.Column(database.String(50))
    surname = database.Column(database.String(50))
    email = database.Column(database.String(50), unique=True)
    password = database.Column(database.String(50))
    visible = database.Column(database.BOOLEAN, default=True)


    def __init__(self, email, password, name, surname):
        self.name = name
        self.surname = surname
        self.email = email
        self.password = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password, password)

class UserToken(database.Model):

    id = database.Column(database.Integer, autoincrement=True, primary_key=True)
    user_id = database.Column(database.Integer)
    access_token = database.Column(database.String(100))
    refresh_token = database.Column(database.String(100))
    #expires_at = database.Column(database.DATETIME, server_default=database.func.now(), onupdate=database.func.now())

    def __init__(self, user_id, access_token, refresh_token):
        self.user_id = user_id
        self.access_token = access_token
        self.refresh_token = refresh_token

class Account(database.Model):
    __tablename__ = 'account'
    id = database.Column(database.Integer, autoincrement=True, primary_key=True)
    public_key = database.Column(database.String(50))
    bank = database.Column(database.String(50))
    branch = database.Column(database.String(50))
    iban = database.Column(database.String(50))
    suffix = database.Column(database.Integer)
    user_id = database.Column(database.Integer, nullable=False)
    host_account = database.Column(database.Integer, default=False)

    def __init__(self, user_id, bank, branch, iban, host_account):
        self.public_key = uuid.uuid4().hex
        self.user_id = user_id
        self.bank = bank
        self.branch = branch
        self.iban = iban
        self.host_account = host_account


    @property
    def serialize(self):
        return dict(public_key=self.public_key, bank=self.bank, branch=self.branch, iban=self.iban)

class Transaction(database.Model):

    __tablename__ = 'transaction'

    id = database.Column(database.Integer, autoincrement=True, primary_key=True)

    public_key = database.Column(database.String(50))

    amount = database.Column(database.FLOAT)

    created_at = database.Column(database.DATETIME, server_default=database.func.now())
    updated_at = database.Column(database.DATETIME, server_default=database.func.now(), onupdate=database.func.now())

    visible = database.Column(database.Boolean, default=True)

    from_user = database.Column(database.Integer, nullable=False)
    to_user = database.Column(database.Integer, nullable=False)
    middle_user = database.Column(database.Integer, nullable=False)


    from_user_account = database.Column(database.Integer,  nullable=False)

    middle_user_to_account = database.Column(database.Integer,  nullable=False)
    middle_user_from_account = database.Column(database.Integer,  nullable=False)

    to_user_account = database.Column(database.Integer,  nullable=False)


    def __init__(self):
        self.public_key = uuid.uuid4().hex


class BankAccount(database.Model):

    id = database.Column(database.Integer, autoincrement=True, primary_key=True)
    iban = database.Column(database.String(100))
    amount = database.Column(database.Float)


    def __init__(self, iban, amount):
        self.iban = iban
        self.amount = amount

