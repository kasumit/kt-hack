from app import app
from decorators import Security as security
from flask import jsonify, request
from service import *
import base64
import json
from flask_cors import cross_origin


user_service = UserService()
account_service = AccountService()
kt_service = KTService()


@app.route('/', methods=['GET'])
def index():
    return 'server up'

@app.route('/api/v1/sign-in', methods=['POST'])
def sign_in():

    parameters = request.get_json(silent=True, force=True)
    email = parameters.get('email')
    password = parameters.get('password')

    if email is None or password is None:
        return jsonify(status=False, message="Email or password cannot be empty"), 400
    else:
        return user_service.sign_in(email, password)


@app.route('/api/v1/sign-up', methods=['POST'])
def sign_up():
    parameters = request.get_json(silent=True, force=True)

    email = parameters.get('email')
    name = parameters.get('name')
    surname = parameters.get('surname')
    password = parameters.get('password')
    password2 = parameters.get('password2')

    if email is None or password is None or password2 is None or name is None or surname is None:
        return jsonify(status=False, message="Empty field, cannot registered"), 400
    elif password2 != password:
        return jsonify(status=False, message="Passwords are not matched"), 400
    else:
        return user_service.sign_up(email, password, name, surname)


@app.route('/api/v1/me/account', methods=['POST'])
@security.access_token_required
def create_account():

    access_token = request.args.get("access_token")

    parameters = request.get_json(silent=True, force=True)

    bank = parameters.get('bank')
    branch = parameters.get('branch')
    iban = parameters.get('iban')
    host_account = parameters.get('host')

    if bank is None or branch is None or iban is None:
        return jsonify(status=False, message="Empty field, cannot registered"), 400
    else:
        return account_service.create_account(access_token, bank, branch, iban, host_account)




@app.route('/api/v1/me/accounts', methods=['GET'])
@security.access_token_required
def get_account():

    access_token = request.args.get("access_token")
    return account_service.get_accounts(token=access_token)


@app.route('/api/v1/me/host-accounts', methods=['GET'])
@security.access_token_required
def get_host_account():
    access_token = request.args.get("access_token")
    return account_service.get_host_accounts(token=access_token)




@app.route('/api/v1/me', methods=['POST'])
@security.access_token_required
def get_me():
    access_token = request.args.get("access_token")
    who = user_service.get_user_profile(access_token)
    return who


@app.route('/api/v1/auth', methods=['POST'])
@security.access_token_required
def auth():
    access_token = request.args.get("access_token")
    who = user_service.find_user_by_access_token(access_token)
    return jsonify(emil=who.email)

#https://idprep.kuveytturk.com.tr/api/connect/authorize?client_id=b5d6edbf3d944a81a9ead3e8d286cf81&scope=accounts%20transfers%20offline_access&response_type=code&redirect_uri=http://127.0.0.1:8100

@app.route('/api/v1/tokens', methods=['POST'])
@cross_origin()
def redirect():

    parameters = request.get_json(silent=True, force=True)
    code = parameters.get('code')
    state = parameters.get('state')

    #code = request.args.get("code")
    #state = request.args.get('state')
    #state = base64.b64decode(state)
    #state = json.loads(state.decode('utf-8'))
    #print(state)

    #
    # print(state)
    # print(state['user_token'])

    return kt_service.get_tokens(code)




@app.route('/api/v1/kt-accounts', methods=['POST'])
def accounts():

    parameters = request.get_json(silent=True, force=True)
    at = parameters.get('access_token')

    #
    # print(state)
    # print(state['user_token'])

    return kt_service.get_accounts(at)



@app.route('/api/v1/kt-transfer', methods=['POST'])
def transfer():

    parameters = request.get_json(silent=True, force=True)
    token = parameters.get('token')
    suffix = parameters.get('suffix')
    reciever_name = parameters.get('reciever_name')
    iban = parameters.get('iban')
    amount = parameters.get('amount')
    comment = parameters.get('comment')
    type_id = parameters.get('type_id')

    #
    # print(state)
    # print(state['user_token'])

    return kt_service.transfer_to_iban(token, suffix, reciever_name, iban, amount, comment, type_id)




@app.route('/api/v1/kt-otp', methods=['POST'])
def otp():

    parameters = request.get_json(silent=True, force=True)
    token = parameters.get('token')
    otp = parameters.get('SmsOtp')
    otpid = parameters.get('secureOtpId')
    #
    # print(state)
    # print(state['user_token'])

    return kt_service.otp_verify(token, otp, otpid)


@app.route("/api/v1/host", methods=['POST'])
def host():
    return jsonify(access_token="083930963dcbb7055dbbbbff710987c2af8efaa2c1466bf2965a7690c3f0924b", iban="TR480020500000007789800001", suffix="2", name="MAHSUN RAZI")
