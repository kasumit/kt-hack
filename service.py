from model import *
from flask import jsonify
from app import database
import requests
from requests.auth import HTTPBasicAuth
import jwt
import codecs
import json
import base64
import random


class UserService:

    def __does_exist_email(self, email):
        user = User.query.filter_by(email=email).first()
        return user is not None

    def get_user_by_email(self, email):

        user = User.query.filter_by(email=email).first()

        return user

    def find_user_by_access_token(self, access_token):
        user_info = jwt.decode(access_token, 'zizu', algorithms=['HS256'])
        return self.get_user_by_email(user_info['email'])

    def does_exist(self, email):

        user = User.query.filter_by(email=email).first()

        return user is not None

    def sign_in(self, email, password):

        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            access_token = jwt.encode({'email': email}, 'zizu', algorithm='HS256')
            return jsonify(status=True, message="Logged in",  access_token=access_token.decode(), state=access_token.decode()), 200

        else:
            return jsonify(status=False, message="Email or password is incorrect"), 400


    def sign_up(self, email, password, name, surname):

        if not self.__does_exist_email(email):
            try:
                user = User(email, password, name, surname)
                database.session.add(user)
                database.session.commit()
                return jsonify(status=True, message="Successfully registered"), 200
            except Exception as e:
                print(e)
                return jsonify(status=False, message="Error occurred while registration process"), 400

        else:
            return jsonify(status=False, message="Email has already exist"), 409



    def get_user_profile(self, token):

        user = self.find_user_by_access_token(token)

        return jsonify(name=user.name, surname=user.surname, email=user.email)

    def set_tokens(self, token, access_token, refresh_token):

        user = self.find_user_by_access_token(token)
        user_token = UserToken.query.filter_by(user_id=user.id).first()
        if user_token is not None:
            user_token.access_token = access_token
            user_token.refresh_token = refresh_token

        else:
            user_token = UserToken(user.id, access_token, refresh_token)
            database.session.add(user_token)
        database.session.commit()


class AccountService:

    def __init__(self):
        self.user_service = UserService()

    def create_account(self, token, bank, branch, iban, host_account):

        try:
            user = self.user_service.find_user_by_access_token(token)

            account = Account.query.filter_by(user_id=user.id, iban=iban, host_account=host_account).first()
            if account is not None:
                return jsonify(public_key=account.public_key, status=False, message="Account is already in the list")
            account = Account(user.id, bank, branch, iban, host_account)
            database.session.add(account)
            database.session.commit()
            return jsonify(public_key=account.public_key, status=True, message="Successfully saved")

        except Exception as e:

            print(e)

        return jsonify(status=False, message="Account is not saved")



    def get_accounts(self, token):
        try:
            user = self.user_service.find_user_by_access_token(token)
            accounts_of_user = Account.query.filter_by(user_id=user.id, host_account=True).all()
            accounts_of_user = list(map(lambda account: account.serialize, accounts_of_user))

            return jsonify(accounts=accounts_of_user, status=True, message="Accounts are fetched")

        except Exception as e:
            print(e)

        return jsonify(accounts=[], status=False, message="No such a account is found")

    def get_host_accounts(self, token):
        try:
            user = self.user_service.find_user_by_access_token(token)
            accounts_of_user = Account.query.filter_by(user_id=user.id, host_account=True).all()
            accounts_of_user = list(map(lambda account: account.serialize, accounts_of_user))

            return jsonify(accounts=accounts_of_user, status=True, message="Accounts are fetched")

        except Exception as e:
            print(e)

        return jsonify(accounts=[], status=False, message="No such a account is found")


class KTService:


    def __init__(self):
        self.url = 'https://apitest.kuveytturk.com.tr/hackathon/v1'
        self.auth_url = 'https://idprep.kuveytturk.com.tr/api/connect'
        self.user_service = UserService()

    def get_tokens(self, token):
        req_data = dict(grant_type="authorization_code", code=token, redirect_uri="http://127.0.0.1:8100", client_id='b5d6edbf3d944a81a9ead3e8d286cf81', client_secret="Yl8IGl7JBC/Vo3ywgrj6m/OHtcdCL1QaaT66xUtuaiTeDOi9JAB6hQ==")
        #req = requests.post("{}/token".format(self.auth_url), headers={'Content-Type': 'application/x-www-form-urlencoded', "Authorization": 'Basic ZDE5YjczYjZjN2QyNDc3MThkMWQyMTg3YzdjNjlmNjc6RXZXMkJYYVQwSkV1UDlJcHBVbGQ4QnNrZ2xwVEZnMnkzT0dSV1hpRE5uTHlxZzR0anc0bXB3PT0='}, data=req_data)
        req = requests.post("{}/token".format(self.auth_url), headers={'Content-Type': 'application/x-www-form-urlencoded'}, data=req_data)
        print(req)
        print(req.status_code)
        if req.status_code != 200:
            return jsonify(status=False, message='Authentication is required')
        return jsonify(status=True, access_token=req.json()['access_token'])



    def get_accounts(self, token):

        # user = self.user_service.find_user_by_access_token(token)
        # if user is None:
        #     return jsonify(accounts=[], status=False, message='User is not found'), 400
        # user_tokens = UserToken.query.filter_by(user_id=user.id).first()
        # if user_tokens is None:
        #     return jsonify(accounts=[], status=False, message='User Tokens are not found'), 401
        # print(user_tokens.access_token)
        resp = requests.get('{}/accounts'.format(self.url), headers={"Authorization": 'Bearer {}'.format(token)})

        return jsonify(resp.json())

    def transfer_to_iban(self, token, suffix, reciever_name, iban, amount, comment, type_id):

        url = "https://apitest.kuveytturk.com.tr/hackathon/v1/transfers/toIBAN"


        payload = '{"SenderAccountSuffix":' + str(suffix) + ', "ReceiverName": "' + reciever_name + '", "ReceiverIBAN": "' + iban  + '", "Amount":' + str(amount) + ', "Comment": "' + comment + '", "PaymentTypeId": ' + str(type_id) + '}'


        #payload = '{"SenderAccountSuffix":' + + ', "ReceiverName": "{}","ReceiverIBAN": "{}","Amount": {}, "Comment": "{}", "PaymentTypeId": {}}'.format(suffix, reciever_name, iban, amount, comment, type_id)

        #payload = "{'SenderAccountSuffix': {}, 'ReceiverName': '{}','ReceiverIBAN': '{}','Amount': {}, 'Comment': '{}', 'PaymentTypeId': {}}".format(suffix, reciever_name, iban, amount, comment, type_id)


        #payload = "{\n        \"SenderAccountSuffix\": {},\n        \"ReceiverName\": \"{}\",\n        \"ReceiverIBAN\": \"{}\",\n        \"Amount\": {},\n        \"Comment\": \"{}\",\n        \"PaymentTypeId\": {}\n    }\n".format(suffix, reciever_name, iban, amount, comment, type_id)
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer {}".format(token),
            'Cache-Control': "no-cache"

        }

        response = requests.request("POST", url, data=payload, headers=headers)
        return jsonify(response.json())



    def otp_verify(self, token, sms_otp, secure_otp_id):

        url = "https://apitest.kuveytturk.com.tr/hackathon/v1/transfers/execute"
        payload = '{"SmsOtp":' + str(sms_otp) + ', "secureOtpId": "' + secure_otp_id + '"}'
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer {}".format(token),
            'Cache-Control': "no-cache"
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        return jsonify(response.json())

class BankAccountService:

    def __init__(self):
        pass

    def get_bank(self, index):

        return [
            {"code":'00135', "name": 'Anadolu Bank'},
            {"code":'00134', "name": 'Denizbank'},
            {"code":'00111', "name": 'Finansbank'},
            {"code":'00012', "name": 'Halkbank'},
            {"code":'00062', "name": 'Garanti Bankasi'},
            {"code":'00146', "name": 'Odea Bank'},
            {"code":'00123', "name": 'HSBC'},
            {"code":'00103', "name": 'Fibabanka'}
        ]

    def random_account_generate(self):
        bank = random.randint(1, 8)
        max = random.randint(0, 5)

        for i in range(max):
            iban = 'TR' + '00' + self.get_bank(bank)['code'] + str(random.randint(1000, 9999)) + str(random.randint(1000, 9999)) + str(random.randint(1000, 9999)) + str(random.randint(1000, 9999))
            branch = "Test Brahch " + i
            bank_name =  self.get_bank(bank)['name']

